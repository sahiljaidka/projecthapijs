/**
 * Created by SONY on 5/29/2016.
 */
'use strict';

var Models = require('../Models');

var updateBooksUser = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.BooksUser.update(criteria, dataToSet, options, callback);
};
//Insert User in DB
var createBooksUser = function (objToSave, callback) {
    new Models.BooksUser(objToSave).save(callback)
};
//Delete User in DB
var deleteBooksUser = function (criteria, callback) {
    Models.BooksUser.remove(criteria, callback);
};

//Get Users from DB
var getBooksUser = function (criteria, projection, options, callback) {
    options.lean = true;
    Models.BooksUser.find(criteria, projection, options, callback);
};

//Get All Generated Codes from DB
var getAllGeneratedCodes = function (callback) {
    var criteria = {
        OTPCode: {$ne: null}
    };
    var projection = {
        OTPCode: 1
    };
    var options = {
        lean: true
    };
    Models.BooksUser.find(criteria, projection, options, function (err, dataAry) {
        if (err) {
            callback(err)
        } else {
            var generatedCodes = [];
            if (dataAry && dataAry.length > 0) {
                dataAry.forEach(function (obj) {
                    generatedCodes.push(obj.OTPCode.toString())
                });
            }
            callback(null, generatedCodes);
        }
    })
};

module.exports = {
    updateBooksUser: updateBooksUser,
    createBooksUser: createBooksUser,
    deleteBooksUser: deleteBooksUser,
    getBooksUser: getBooksUser,
    getAllGeneratedCodes: getAllGeneratedCodes
};
