/**
 * Created by SONY on 4/3/2016.
 */
'use strict';

var Models = require('../Models');


//Insert User in DB
var createAddress = function (objToSave, callback) {
    new Models.CustomerAddress(objToSave).save(callback)
};
//Delete User in DB
var deleteAddress = function (criteria, callback) {
    Models.CustomerAddress.findOneAndRemove(criteria, callback);
};

//Get Users from DB
var getAddress = function (criteria, projection, options, callback) {
    options.lean = true;
    Models.CustomerAddress.find(criteria, projection, options, callback);
};

var updateAddress = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.CustomerAddress.findOneAndUpdate(criteria, dataToSet, options, callback);
};

module.exports = {
    createAddress: createAddress,
    deleteAddress: deleteAddress,
    getAddress: getAddress,
    updateAddress: updateAddress
};
