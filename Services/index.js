/**
 * Created by SONY on 3/24/2016.
 */
module.exports = {
    CustomerService: require('./CustomerService'),
    StatusService: require('./StatusService'),
    AdminService: require('./AdminService'),
    AddressService: require('./AddressServices'),
    BookingService: require('./BookingService'),
    BooksUserService: require('./BooksUserServices')
};