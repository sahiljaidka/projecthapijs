var Joi = require('joi');
var async = require('async');
var MD5 = require('md5');
var Boom = require('boom');
var Models = require('../Models');
var Config = require('../Config');
var validator = require('validator');
var randomstring = require("randomstring");

var CryptData = function (stringToCrypt) {
    return MD5(MD5(stringToCrypt));
};

var getTimestamp = function (inDate) {
    if (inDate)
        return new Date();
    return new Date().toISOString();
};

var failActionFunction = function (request, reply, source, error) {
    console.log(error);
    var customErrorMessage = '';
    if (error.output.payload.message.indexOf("[") > -1) {
        customErrorMessage = error.output.payload.message.substr(error.output.payload.message.indexOf("["));
    } else {
        customErrorMessage = error.output.payload.message;
    }
    customErrorMessage = customErrorMessage.replace(/"/g, '');
    customErrorMessage = customErrorMessage.replace('[', '');
    customErrorMessage = customErrorMessage.replace(']', '');
    error.output.payload.message = customErrorMessage;
    delete error.output.payload.validation
    return reply(error);
};

var authorizationHeaderObj = Joi.object({
    authorization: Joi.string().required()
}).unknown();

var deleteUnnecessaryUserData = function (userObj) {
    delete userObj['accessToken'];
    return userObj;
};

var sendSuccess = function (successMsg, data) {
    successMsg = successMsg || Config.AppConstants.STATUS_MSG.SUCCESS.DEFAULT.customMessage;
    if (typeof successMsg == 'object' && successMsg.hasOwnProperty('statusCode') && successMsg.hasOwnProperty('customMessage')) {
        return {statusCode: successMsg.statusCode, message: successMsg.customMessage, data: data || null};

    } else {
        return {statusCode: 200, message: successMsg, data: data || null};

    }
};

var verifyEmailFormat = function (string) {
    return validator.isEmail(string)
};

var sendError = function (data) {
    //console.trace('ERROR OCCURED ', data);
    if (typeof data == 'object' && data.hasOwnProperty('statusCode') && data.hasOwnProperty('customMessage')) {
        console.log('attaching resposnetype', data.type);
        var errorToSend = Boom.create(data.statusCode, data.customMessage);
        errorToSend.output.payload.responseType = data.type;
        return errorToSend;
    } else {
        var errorToSend = '';
        if (typeof data == 'object') {
            if (data.name == 'MongoError') {
                errorToSend += Config.AppConstants.STATUS_MSG.ERROR.DB_ERROR.customMessage;
                if (data.code = 11000) {
                    var duplicateValue = data.errmsg && data.errmsg.substr(data.errmsg.lastIndexOf('{ : "') + 5);
                    duplicateValue = duplicateValue.replace('}', '');
                    errorToSend += Config.AppConstants.STATUS_MSG.ERROR.DUPLICATE.customMessage + " : " + duplicateValue;
                    if (data.message.indexOf('customer_1_streetAddress_1_city_1_state_1_country_1_zip_1') > -1) {
                        errorToSend = Config.AppConstants.STATUS_MSG.ERROR.DUPLICATE_ADDRESS.customMessage;
                    }
                }
            } else if (data.name == 'ApplicationError') {
                errorToSend += Config.AppConstants.STATUS_MSG.ERROR.APP_ERROR.customMessage + ' : ';
            } else if (data.name == 'ValidationError') {
                errorToSend += Config.AppConstants.STATUS_MSG.ERROR.APP_ERROR.customMessage + data.message;
            } else if (data.name == 'CastError') {
                errorToSend += Config.AppConstants.STATUS_MSG.ERROR.DB_ERROR.customMessage + Config.AppConstants.STATUS_MSG.ERROR.INVALID_ID.customMessage + data.value;
            }
        } else {
            errorToSend = data
        }
        var customErrorMessage = errorToSend;
        if (typeof customErrorMessage == 'string') {
            if (errorToSend.indexOf("[") > -1) {
                customErrorMessage = errorToSend.substr(errorToSend.indexOf("["));
            }
            customErrorMessage = customErrorMessage && customErrorMessage.replace(/"/g, '');
            customErrorMessage = customErrorMessage && customErrorMessage.replace('[', '');
            customErrorMessage = customErrorMessage && customErrorMessage.replace(']', '');
        }
        return Boom.create(400, customErrorMessage)
    }
};

var validateLatLongValues = function (lat, long) {
    var valid = true;
    if (lat < -90 || lat > 90) {
        valid = false;
    }
    if (long < -180 || long > 180) {
        valid = false;
    }
    return valid;
};
var getFileNameWithUserIdWithCustomPrefix = function (thumbFlag, fullFileName, type, userId) {
    console.log("userId241", userId);
    var prefix = '';
    if (type == Config.AppConstants.DATABASE.FILE_TYPES.LOGO) {
        prefix = Config.AppConstants.DATABASE.LOGO_PREFIX.ORIGINAL;
    } else if (type == Config.AppConstants.DATABASE.FILE_TYPES.DOCUMENT) {
        prefix = Config.AppConstants.DATABASE.DOCUMENT_PREFIX;
    }
    var ext = fullFileName && fullFileName.length > 0 && fullFileName.substr(fullFileName.lastIndexOf('.') || 0, fullFileName.length);
    if (thumbFlag && type == Config.AppConstants.DATABASE.FILE_TYPES.LOGO) {
        prefix = Config.AppConstants.DATABASE.LOGO_PREFIX.THUMB;
    }
    var rand = generateRandomString();
    return prefix + userId + rand + ext;
};

var getFileNameWithUserId = function (thumbFlag, fullFileName, userId) {
    var prefix = Config.AppConstants.DATABASE.PROFILE_PIC_PREFIX.ORIGINAL;
    var ext = fullFileName && fullFileName.length > 0 && fullFileName.substr(fullFileName.lastIndexOf('.') || 0, fullFileName.length);
    if (thumbFlag) {
        prefix = Config.AppConstants.DATABASE.PROFILE_PIC_PREFIX.THUMB;
    }
    var rand = generateRandomString();
    return prefix + userId + rand + ext;
};

var generateRandomString = function () {
    return randomstring.generate(7);
};

var generateFilenameWithExtension = function generateFilenameWithExtension(oldFilename, newFilename) {
    var ext = oldFilename.substr((~-oldFilename.lastIndexOf(".") >>> 0) + 2);
    return newFilename + '.' + ext;
}
module.exports = {
    CryptData: CryptData,
    getTimestamp: getTimestamp,
    failActionFunction: failActionFunction,
    authorizationHeaderObj: authorizationHeaderObj,
    deleteUnnecessaryUserData: deleteUnnecessaryUserData,
    sendSuccess: sendSuccess,
    sendError: sendError,
    verifyEmailFormat: verifyEmailFormat,
    validateLatLongValues: validateLatLongValues,
    getFileNameWithUserIdWithCustomPrefix: getFileNameWithUserIdWithCustomPrefix,
    getFileNameWithUserId: getFileNameWithUserId,
    generateRandomString: generateRandomString,
    generateFilenameWithExtension:generateFilenameWithExtension
};