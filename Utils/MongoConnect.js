/**
 * Created by SONY on 3/12/2016.
 */
'use strict';
var Mongoose = require('mongoose');
var Config = require('../Config');

//Connect to MongoDB
Mongoose.connect('mongodb://localhost/yojek', function (err) {
    if (err) {
        console.log("DB Error: ", err);
        process.exit(1);
    } else {
        console.log('MongoDB Connected');
    }
});

exports.Mongoose = Mongoose;