'use strict';

var Jwt = require('jsonwebtoken');
var async = require('async');
var Models = require('../Models');
var Config = require('../Config');

var setTokenInDB = function (userId, userType, tokenToSave, callback) {
    var criteria = {
        _id: userId
    };
    var setQuery = {
        accessToken: tokenToSave
    };
    async.series([
        function (cb) {
            switch (userType) {
                case Config.AppConstants.DATABASE.USER_ROLES.CUSTOMER:
                    Models.Customers.findOneAndUpdate(criteria, setQuery, {new: true}, function (err, dataAry) {
                        if (err) {
                            cb(err)
                        } else {
                            if (dataAry && dataAry._id) {
                                cb();
                            } else {
                                cb("Implementation Error")
                            }
                        }
                    });
                    break;
                case Config.AppConstants.DATABASE.USER_ROLES.ADMIN:
                    Models.Admin.findOneAndUpdate(criteria, setQuery, {new: true}, function (err, dataAry) {
                        if (err) {
                            cb(err)
                        } else {
                            if (dataAry && dataAry._id) {
                                cb();
                            } else {
                                cb("Implementation Error")
                            }
                        }
                    });
                    break;
            }
        }
    ], function (err, result) {
        if (err) {
            callback(err)
        } else {
            callback()
        }

    });
};

var setToken = function (tokenData, callback) {
    if (!tokenData.id || !tokenData.type) {
        callback("Imp error");
    } else {
        var tokenToSend = Jwt.sign(tokenData, "secretkey");
        setTokenInDB(tokenData.id, tokenData.type, tokenToSend, function (err, data) {
            console.log('token>>>>', err, data);
            callback(err, {accessToken: tokenToSend})
        })
    }
};

var getTokenFromDB = function (userId, userType, token, callback) {
    console.log("%%%%%%%%%%%%", userId, userType, token);
    var userData = null;
    var criteria = {
        _id: userId,
        accessToken: token
    };
    async.series([
        function (cb) {
            switch (userType) {
                case Config.AppConstants.DATABASE.USER_ROLES.CUSTOMER :
                    Models.Customers.find(criteria, {}, {lean: true}, function (err, dataAry) {
                        console.log("$$$$$$$$$$$$$$", err, dataAry);
                        if (err) {
                            cb(err)
                        } else {
                            if (dataAry && dataAry.length > 0) {
                                userData = dataAry[0];
                                cb();
                            } else {
                                cb("Invalid Token")
                            }
                        }
                    });
                    break;
                case Config.AppConstants.DATABASE.USER_ROLES.ADMIN :
                    Models.Admin.find(criteria, {}, {lean: true}, function (err, dataAry) {
                        console.log("$$$$$$$$$$$$$$", err, dataAry);
                        if (err) {
                            cb(err)
                        } else {
                            if (dataAry && dataAry.length > 0) {
                                userData = dataAry[0];
                                cb();
                            } else {
                                cb("Invalid Token")
                            }
                        }
                    });
                    break;
            }
        }
    ], function (err, result) {
        if (err) {
            callback(err)
        } else {
            if (userData && userData._id) {
                userData.id = userData._id;
            }
            callback(null, {userData: userData})
        }

    });
};


var verifyToken = function (token, callback) {
    var response = {
        valid: false
    };
    Jwt.verify(token, "secretkey", function (err, decoded) {
        console.log('jwt err', err, decoded);
        if (err) {
            callback(err)
        } else {
            getTokenFromDB(decoded.id, decoded.type, token, callback);
        }
    });
};
module.exports = {
    setToken: setToken,
    verifyToken: verifyToken
};