/**
 * Created by clicklabs113 on 3/10/16.
 */

var CustomerRoute = require('./CustomerRoute');
var AdminNOC = require('./AdminNOC');
var BookingRoute = require('./BookingRoute');
var BooksUserRoute = require('./BooksUserRoute');

var all = [].concat(CustomerRoute, AdminNOC, BookingRoute, BooksUserRoute);
module.exports = all;