/**
 * Created by SONY on 5/29/2016.
 */
'use strict';

const uuid = require('node-uuid');
const Joi = require('joi');
const Controllers = require('../Controllers');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var Config = require('../Config');


var booksUserRegister = {
    method: 'POST',
    path: '/api/booksUser/register',
    handler: function (request, reply) {
        var payload = request.payload;
        Controllers.BooksUserCtrl.booksUserReg(payload, function (err, data) {
            if (err) {
                reply(UniversalFunctions.sendError(err));
                //reply(err);
            } else {
                reply(UniversalFunctions.sendSuccess(Config.AppConstants.STATUS_MSG.SUCCESS.CREATED, data)).code(201);
                //reply(null, data);
            }
        });
    },
    config: {
        payload: {
            maxBytes: 20715200,
            output: 'file',
            parse: true,
            allow: 'multipart/form-data'
        },
        validate: {
            payload: {
                fullName: Joi.string().regex(/^[a-zA-Z ]+$/).min(2).required(),
                email: Joi.string().required().email(),
                password: Joi.string().required().min(6),
                countryCode: Joi.string().required(),
                mobileNumber: Joi.string().min(5).regex(/^[0-9]+$/).required(),
                //deviceToken: Joi.string().required(),
                //deviceType: Joi.string().required().valid([Config.AppConstants.DATABASE.DEVICETYPE.IOS, Config.AppConstants.DATABASE.DEVICETYPE.ANDROID]),
                gender: Joi.string().required().valid([Config.AppConstants.DATABASE.GENDER.MALE, Config.AppConstants.DATABASE.GENDER.FEMALE]),
                profilePic: Joi.any()
                    .meta({swaggerType: 'file'})
                    .allow('')
                    .description('image file')
            },
            failAction: UniversalFunctions.failActionFunction
        },
        description: 'Register BooksUser',
        tags: ['api', 'BooksUser'],
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

module.exports = [
    booksUserRegister
];