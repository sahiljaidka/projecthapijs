/**
 * Created by SONY on 3/30/2016.
 */

'use strict';

const uuid = require('node-uuid');
const Joi = require('joi');
const Controllers = require('../Controllers');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var Config = require('../Config');

var swaggerDefaultResponseMessages = [
    {code: 200, message: 'OK'},
    {code: 400, message: 'Bad Request'},
    {code: 401, message: 'Unauthorized'},
    {code: 404, message: 'Data Not Found'},
    {code: 500, message: 'Internal Server Error'}
];

var adminRegister = {
    method: 'POST',
    path: '/api/admin/register',
    handler: function (request, reply) {
        var payload = request.payload;
        Controllers.AdminNOC.adminRegister(payload, function (err, data) {
            if (err) {
                reply(UniversalFunctions.sendError(err));
                //reply(err);
            } else {
                reply(UniversalFunctions.sendSuccess(Config.AppConstants.STATUS_MSG.SUCCESS.CREATED, data)).code(201);
                //reply(null, data);
            }
        });
    },
    config: {
        validate: {
            payload: {
                fullName: Joi.string().regex(/^[a-zA-Z ]+$/).min(2).required(),
                email: Joi.string().required().email(),
                password: Joi.string().required().min(6),
                phoneNumber: Joi.string().min(5).regex(/^[0-9]+$/).required(),
                role: Joi.string().required().valid([Config.AppConstants.DATABASE.ROLES.CALL_CENTER,
                    Config.AppConstants.DATABASE.ROLES.FACILITATOR, Config.AppConstants.DATABASE.ROLES.FINANCE,
                    Config.AppConstants.DATABASE.ROLES.MARKETING_REPOS, Config.AppConstants.DATABASE.ROLES.SUPER_ADMIN])

            },
            failAction: UniversalFunctions.failActionFunction
        },
        description: 'Register Admin',
        tags: ['api', 'Admin'],
        plugins: {
            'hapi-swagger': {
                responseMessages: swaggerDefaultResponseMessages
            }
        }
    }
};

var adminLogin = {
    method: 'POST',
    path: '/api/admin/login',
    handler: function (request, reply) {
        var payload = request.payload;
        console.log(request.payload.email);
        Controllers.AdminNOC.adminLogin(payload, function (err, data) {
            if (err) {
                reply(UniversalFunctions.sendError(err));
                //reply(err);
            } else {
                reply(UniversalFunctions.sendSuccess(null, data));
                //reply(null, data);
            }
        });
    },
    config: {
        description: 'Login for Admin',
        tags: ['api', 'Admin'],
        validate: {
            payload: {
                email: Joi.string().required(),
                password: Joi.string().required().min(6),
                role: Joi.string().required().valid([Config.AppConstants.DATABASE.ROLES.CALL_CENTER,
                    Config.AppConstants.DATABASE.ROLES.FACILITATOR, Config.AppConstants.DATABASE.ROLES.FINANCE,
                    Config.AppConstants.DATABASE.ROLES.MARKETING_REPOS, Config.AppConstants.DATABASE.ROLES.SUPER_ADMIN])
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: swaggerDefaultResponseMessages
            }
        }
    }
};

var getProfile = {
    method: 'GET',
    path: '/api/admin/getProfile',
    config: {
        description: 'Get profile of Admin',
        auth: 'UserAuth',
        tags: ['api', 'Admin'],
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            console.log("-----route------>>" + JSON.stringify(userData));
            if (userData && userData.id) {
                Controllers.AdminNOC.getProfile(userData, function (err, success) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                        //reply(err);
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, success));
                        //reply(userData);
                    }
                });
            } else {
                reply(UniversalFunctions.sendError(Config.AppConstants.STATUS_MSG.ERROR.INVALID_TOKEN));
                //reply("Invalid Token");
            }
        },
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: swaggerDefaultResponseMessages
            }
        }
    }
};

var changePassword = {
    method: 'PUT',
    path: '/api/admin/changePassword',
    handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controllers.AdminNOC.changePassword(request.payload, userData, function (err, data) {
            if (!err) {
                reply(UniversalFunctions.sendSuccess(Config.AppConstants.STATUS_MSG.SUCCESS.PASSWORD_RESET, data));
                //return reply("Password Changed Successfully", data);
            }
            else {
                reply(UniversalFunctions.sendError(err));
                //return reply(err);
            }
        });
    },
    config: {
        description: 'Change Password',
        tags: ['api', 'Admin'],
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            payload: {
                oldPassword: Joi.string().required().min(6),
                newPassword: Joi.string().required().min(6)
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: swaggerDefaultResponseMessages
            }
        }
    }
};

var getAllCustomers = {
    method: 'GET',
    path: '/api/admin/getAllCustomers',
    handler: function (request, reply) {
        Controllers.AdminNOC.getAllCustomer(function (err, data) {
            if (err) {
                reply(err);
            } else {
                reply(null, data);
            }
        });
    },
    config: {
        description: 'Get all Customers',
        tags: ['api', 'Admin'],
        validate: {
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: swaggerDefaultResponseMessages
            }
        }
    }
};

var searchCustomer = {
    method: 'GET',
    path: '/api/admin/searchCustomer',
    config: {
        description: 'Search Customer',
        tags: ['api', 'Admin'],
        validate: {
            query: {firstName: Joi.string()},
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: swaggerDefaultResponseMessages
            }
        }
    },
    handler: function (request, reply) {
        //console.log("+++++++++++++++++++++",request.query.name);
        Controllers.AdminNOC.searchCustomer(request.query.firstName, function (err, data) {
            if (err) {
                reply(err);
            } else {
                reply(null, data);
            }
        });
    }
};

module.exports = [
    adminRegister,
    adminLogin,
    getProfile,
    changePassword,
    getAllCustomers,
    searchCustomer
];