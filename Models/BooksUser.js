/**
 * Created by SONY on 5/21/2016.
 */

var mongoose = require('mongoose');
var Config = require('../Config');
var Schema = mongoose.Schema;
var device = Config.AppConstants.DATABASE.DEVICETYPE;
var gender = Config.AppConstants.DATABASE.GENDER;


var BooksUser = new Schema({
    fullName: {type: String},
    email: {type: String},
    password: {type: String},
    countryCode: {type: String},
    mobileNumber: {type: String},
    deviceToken: {type: String},
    accessToken: {type: String},
    deviceType: {
        type: String, enum: [
            device.ANDROID,
            device.IOS
        ]
    },
    gender: {
        type: String, enum: [
            gender.FEMALE,
            gender.MALE
        ]
    },
    OTPCode: {type: String, trim: true},
    isPhoneVerified: {type: Boolean, default: false},
    highestQualification: {type: String},
    registrationDateUTC: {type: Date, default: Date.now, required: true},
    profilePicURL: {
        original: {type: String, default: null},
        thumbnail: {type: String, default: null}
    }
});

module.exports = mongoose.model('BooksUser', BooksUser);