/**
 * Created by SONY on 3/30/2016.
 */

var mongoose = require('mongoose');
var Config = require('../Config');
var Schema = mongoose.Schema;
var roles = Config.AppConstants.DATABASE.ROLES;


var Admin = new Schema({
    fullName: {type: String},
    email: {type: String, unique: true},
    password: {type: String},
    phoneNumber: {type: String},
    accessToken: {type: String},
    createdAt: {type: Date, default: Date.now()},
    role: {
        type: String, enum: [
            roles.CALL_CENTER,
            roles.FACILITATOR,
            roles.FINANCE,
            roles.MARKETING_REPOS,
            roles.SUPER_ADMIN
        ]
    },
    isAvailable: {type: Boolean, default: false}
});

module.exports = mongoose.model('Admin', Admin);