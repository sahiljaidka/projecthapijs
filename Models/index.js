module.exports = {
    Customers: require('./Customers'),
    Admin: require('./Admin'),
    CustomerAddress: require('./CustomerAddress'),
    Booking: require('./Booking'),
    BooksUser: require('./BooksUser')
};