/**
 * Created by SONY on 3/12/2016.
 */
module.exports = {
    CustomerController: require('./CustomerController'),
    StatusController: require('./StatusController'),
    AdminNOC: require('./AdminNOC'),
    BookingCtrl: require('./BookingCtrl'),
    BooksUserCtrl: require('./BooksUserCtrl')
};