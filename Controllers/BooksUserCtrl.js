/**
 * Created by SONY on 5/29/2016.
 */
var async = require('async');
var mongoose = require('mongoose');
var Models = require('../Models');
var Config = require('../Config');
var UniversalFunctions = require('../Utils/UniversalFunctions');
const createToken = require('../Utils/Token');
var Services = require('../Services');
var ERROR = Config.AppConstants.STATUS_MSG.ERROR;
var CodeGenerator = require('../Lib/CodeGenerator');
var UploadManager = require('../Lib/UploadManager');
var DAO = require('../DAO/DAO');

var booksUserReg = function (payload, callb) {
    var customerDoesNotExist = false;
    var returnedData = {};
    var accessToken = null;
    var uniqueCode = null;
    var dataToSave = payload;
    var customerData = null;
    var dataToUpdate = {};
    if (dataToSave.password)
        dataToSave.password = UniversalFunctions.CryptData(dataToSave.password);
    payload.email = payload.email.toLowerCase();
    if (payload.profilePic && payload.profilePic.filename) {
        dataToUpdate.profilePicURL = {
            original: null,
            thumbnail: null
        }
    }
    async.series([
        function (cb) {
            //verify email address format
            if (!UniversalFunctions.verifyEmailFormat(dataToSave.email)) {
                cb(ERROR.INVALID_EMAIL);
            } else {
                cb();
            }
        },
        function (cb) {
            var criteria = {
                $or: [{email: payload.email}, {mobileNumber: payload.mobileNumber}]
            };
            Services.BooksUserService.getBooksUser(criteria, {}, {lean: true}, function (err, resp) {
                if (err) {
                    cb(err)
                } else {
                    if (resp && resp.length != 0) {
                        cb(ERROR.USER_ALREADY_REGISTERED)
                    } else {
                        customerDoesNotExist = true;
                        cb()
                    }
                }
            })
        },
        function (cb) {
            CodeGenerator.generateUniqueCode(4, Config.AppConstants.DATABASE.USER_ROLES.CUSTOMER, function (err, numberObj) {
                if (err) {
                    cb(err);
                } else {
                    if (!numberObj || numberObj.number == null) {
                        cb(ERROR.UNIQUE_CODE_LIMIT_REACHED);
                    } else {
                        uniqueCode = numberObj.number;
                        cb();
                    }
                }
            })
        },
        function (cb) {
            //Insert Into DB
            dataToSave.OTPCode = uniqueCode;
            dataToSave.mobileNumber = payload.mobileNumber;
            Services.BooksUserService.createBooksUser(dataToSave, function (err, customerDataFromDB) {
                //Models.Customers(dataToSave).save(function (err, customerDataFromDB) {
                console.log('hello', err, customerDataFromDB);
                if (err) {
                    if (err.code == 11000 && err.message.indexOf('customers.$mobileNo_1') > -1) {
                        cb(ERROR.PHONE_NO_EXIST);

                    } else if (err.code == 11000 && err.message.indexOf('customers.$email_1') > -1) {
                        cb(ERROR.EMAIL_ALREADY_EXIST);

                    } else {
                        cb(err)
                    }
                } else {
                    customerData = customerDataFromDB;
                    cb();
                }
            });
        },

        /*        function (cb) {
         if (payload && customerData._id && dataToSave.profilePic) {
         var document = Config.AppConstants.DATABASE.FILE_TYPES.DOCUMENT;
         UploadManager.uploadFile(dataToSave.profilePic, customerData._id, document, function (err, uploadedInfo) {
         if (err) {
         cb(err)
         }
         var profilePic = uploadedInfo && uploadedInfo.original && Config.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original || null;
         dataToUpdate.profilePic = profilePic;
         return cb();
         });
         } else {
         cb();
         }
         },
         function (cb) {
         var criteria = {_id: customerData._id};
         var update = {
         $set: dataToUpdate
         }
         DAO.findOneAndUpdateData(Models.BooksUser, criteria, update, {new: true}, function (err, data) {
         if (err) {
         return cb(err)
         }
         customerData = data;
         return cb();
         });
         },*/
        function (cb) {
            //Check if profile pic is being updated
            var profilePicURL = {};
            dataToUpdate.profilePicURL = {
                original: Config.awsS3Config.s3BucketCredentials.agentDefaultPicUrl,
                thumbnail: Config.awsS3Config.s3BucketCredentials.agentDefaultPicUrl
            };
            var profilePicture = payload.profilePic;
            if (payload.hasOwnProperty("profilePic") && profilePicture && profilePicture.filename) {
                UploadManager.uploadProfilePicture(profilePicture, Config.awsS3Config.s3BucketCredentials.folder.customer, (payload.mobileNumber).replace('+', '0'), function (err, uploadedInfo) {
                    console.log('update profile pic', err, uploadedInfo);
                    if (err) {
                        cb(err)
                    } else {
                        dataToUpdate.profilePicURL = {
                            original: uploadedInfo.profilePicture,
                            thumbnail: uploadedInfo.profilePictureThumb
                        };
                        cb();
                    }
                });
            }
            else {
                console.log('urls');
                console.log(profilePicURL);
                cb();
            }
        }, function (cb) {
            if (customerData && dataToUpdate && dataToUpdate.profilePicURL && dataToUpdate.profilePicURL.original) {
                //Update User
                var criteria = {
                    _id: customerData._id
                };
                var setQuery = {
                    $set: dataToUpdate
                };
                Services.BooksUserService.updateBooksUser(criteria, setQuery, {new: true}, function (err, updatedData) {
                    customerData = updatedData;
                    cb(err, updatedData)
                })
            }
        },
        function (cb) {
            //Set Access
            if (customerData) {
                var tokenData = {
                    id: customerData._id,
                    type: Config.AppConstants.DATABASE.USER_ROLES.CUSTOMER
                };
                createToken.setToken(tokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        accessToken = output && output.accessToken || null;
                        cb();
                    }
                })
            } else {
                cb(ERROR.IMP_ERROR)
            }
        }
    ], function (err, data) {
        if (err) {
            callb(err)
        } else {
            var details = {
                customerDetails: dataToSave,
                accessToken: accessToken
            };
            return callb(null, details);
        }
    })
};

module.exports = {
    booksUserReg: booksUserReg
};