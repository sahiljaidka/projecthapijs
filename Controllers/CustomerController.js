var async = require('async');
var mongoose = require('mongoose');
var Models = require('../Models');
var Config = require('../Config');
var UniversalFunctions = require('../Utils/UniversalFunctions');
const createToken = require('../Utils/Token');
var Services = require('../Services');
var ERROR = Config.AppConstants.STATUS_MSG.ERROR;
var CodeGenerator = require('../Lib/CodeGenerator');

var customerRegister = function (payload, callb) {
    var customerDoesNotExist = false;
    var returnedData = {};
    var accessToken = null;
    var uniqueCode = null;
    var dataToSave = payload;
    var customerData = null;
    var dataToUpdate = {};
    if (dataToSave.password)
        dataToSave.password = UniversalFunctions.CryptData(dataToSave.password);
    payload.email = payload.email.toLowerCase();
    async.series([
        function (cb) {
            //verify email address
            if (!UniversalFunctions.verifyEmailFormat(dataToSave.email)) {
                cb(ERROR.INVALID_EMAIL);
            } else {
                cb();
            }
        },
        function (cb) {
            var criteria = {
                $or: [{email: payload.email}, {mobileNumber: payload.mobileNumber}]
            };
            Services.CustomerService.getCustomer(criteria, {}, {lean: true}, function (err, resp) {
                if (err) {
                    cb(err)
                } else {
                    if (resp && resp.length != 0) {
                        cb(ERROR.USER_ALREADY_REGISTERED)
                    } else {
                        customerDoesNotExist = true;
                        cb()
                    }
                }
            })
        },
        function (cb) {
            CodeGenerator.generateUniqueCode(4, Config.AppConstants.DATABASE.USER_ROLES.CUSTOMER, function (err, numberObj) {
                if (err) {
                    cb(err);
                } else {
                    if (!numberObj || numberObj.number == null) {
                        cb(ERROR.UNIQUE_CODE_LIMIT_REACHED);
                    } else {
                        uniqueCode = numberObj.number;
                        cb();
                    }
                }
            })
        },
        function (cb) {
            //Insert Into DB
            dataToSave.OTPCode = uniqueCode;
            dataToSave.mobileNumber = payload.mobileNumber;
            Services.CustomerService.createCustomer(dataToSave, function (err, customerDataFromDB) {
                //Models.Customers(dataToSave).save(function (err, customerDataFromDB) {
                console.log('hello', err, customerDataFromDB);
                if (err) {
                    if (err.code == 11000 && err.message.indexOf('customers.$mobileNo_1') > -1) {
                        cb(ERROR.PHONE_NO_EXIST);

                    } else if (err.code == 11000 && err.message.indexOf('customers.$email_1') > -1) {
                        cb(ERROR.EMAIL_ALREADY_EXIST);

                    } else {
                        cb(err)
                    }
                } else {
                    customerData = customerDataFromDB;
                    cb();
                }
            });
        }, function (cb) {
            //Set Access Token
            if (customerData) {
                var tokenData = {
                    id: customerData._id,
                    type: Config.AppConstants.DATABASE.USER_ROLES.CUSTOMER
                };
                createToken.setToken(tokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        accessToken = output && output.accessToken || null;
                        cb();
                    }
                })
            } else {
                cb(ERROR.IMP_ERROR)
            }
        }
    ], function (err, data) {
        if (err) {
            callb(err)
        } else {
            var details = {
                customerDetails: dataToSave,
                accessToken: accessToken
            };
            return callb(null, details);
        }
    })
};

var customerLogin = function (payload, callBackRoute) {
    var userFound = false;
    var successLogin = false;
    var accessToken = null;
    var returnedData = {};
    var updatedUserDetails = null;
    var defaultAddress = null;

    async.series([
        //function (cb) {
        //    //verify email address
        //    if (!UniversalFunctions.verifyEmailFormat(payload.email)) {
        //        cb(ERROR.INVALID_EMAIL);
        //    } else {
        //        cb();
        //    }
        //},
        function (cb) {
            var criteria = {
                $or: [{email: payload.emailOrPhone}, {mobileNumber: payload.emailOrPhone}]
            };
            var option = {
                lean: true
            };
            Services.CustomerService.getCustomer(criteria, {}, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    userFound = result && result[0] || null;
                    returnedData = result;
                    cb();
                }
            });
        },
        function (cb) {
            //validations
            if (!userFound) {
                //cb("You are not registered with us.");
                cb(ERROR.UNREGISTERED_EMAIL)
            } else {
                if (userFound && userFound.isPhoneVerified == false) {
                    cb(ERROR.NOT_VERIFIED)
                }
                else {
                    if (userFound && userFound.password != UniversalFunctions.CryptData(payload.password)) {
                        //cb("Incorrect Password");
                        cb(ERROR.INCORRECT_PASSWORD)
                    } else {
                        successLogin = true;
                        cb();
                    }
                }
            }
        },
        function (cb) {
            var query = {
                customer: userFound._id,
                isDefault: false,
                isDeleted: false
            };
            Services.AddressService.getAddress(query, {}, {lean: true}, function (err, data) {
                if (!err) {
                    if (data.length > 0) {
                        defaultAddress = data[0];
                        cb(null);
                    }
                    else {
                        cb(null);
                    }
                }
                else {
                    cb(err)
                }
            })
        },
        function (cb) {
            if (successLogin) {
                var tokenData = {
                    id: userFound._id,
                    type: Config.AppConstants.DATABASE.USER_ROLES.CUSTOMER
                };
                createToken.setToken(tokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        if (output && output.accessToken) {
                            accessToken = output && output.accessToken;
                            cb();
                        } else {
                            //cb("Error 2")
                            cb(ERROR.IMP_ERROR)
                        }
                    }
                })
            } else {
                cb(ERROR.IMP_ERROR)
                //cb("Error 3")
            }

        }
    ], function (err, data) {
        if (err) {
            callBackRoute(err)
        } else {
            var userDetails = UniversalFunctions.deleteUnnecessaryUserData(userFound);
            var details = {
                customerDetails: userDetails,
                accessToken: accessToken
            };
            callBackRoute(null, details);
        }
    })
};

var getProfile = function (data, callbackRoute) {
    var query = {
        _id: data.id
    };
    var projection = {
        firstName: 1,
        lastName: 1,
        email: 1,
        mobileNumber: 1,
        myStatus: 1
    };
    var options = {lean: true};
    Services.CustomerService.getCustomer(query, projection, options, function (err, data) {
        if (err) {
            callbackRoute(err);
        } else {
            var customerData = data && data[0] || null;
            console.log("customerData-------->>>" + JSON.stringify(customerData));
            if (customerData == null) {
                callbackRoute(ERROR.INVALID_TOKEN);
                //callbackRoute("Invalid Token");
            } else {
                callbackRoute(null, customerData);
            }
        }
    });
};

var changePassword = function (payload, tokenData, callbackRoute) {
    var oldPassword = UniversalFunctions.CryptData(payload.oldPassword);
    var newPassword = UniversalFunctions.CryptData(payload.newPassword);
    async.series([
            function (callback) {
                var query = {
                    _id: tokenData.id
                };
                var projection = {
                    password: 1
                };
                var options = {lean: true};
                Services.CustomerService.getCustomer(query, projection, options, function (err, data) {
                    if (err) {
                        callback(err);
                    } else {
                        var customerData = data && data[0] || null;
                        console.log("customerData-------->>>" + JSON.stringify(customerData));
                        if (customerData == null) {
                            callback(ERROR.NOT_FOUND);
                        } else {
                            if (data[0].password == oldPassword && data[0].password != newPassword) {
                                callback(null);
                            }
                            else if (data[0].password != oldPassword) {
                                callback(ERROR.WRONG_PASSWORD)
                            }
                            else if (data[0].password == newPassword) {
                                callback(ERROR.NOT_UPDATE)
                            }
                        }
                    }
                });
            },
            function (callback) {
                var dataToUpdate = {$set: {'password': UniversalFunctions.CryptData(payload.newPassword)}};
                var condition = {_id: tokenData.id};
                Services.CustomerService.updateCustomer(condition, dataToUpdate, {}, function (err, user) {
                    console.log("customerData-------->>>" + JSON.stringify(user));
                    if (err) {
                        callback(err);
                    } else {
                        if (!user || user.length == 0) {
                            callback(ERROR.NOT_FOUND);
                        }
                        else {
                            callback(null);
                        }
                    }
                });
            }
        ],
        function (error, result) {
            if (error) {
                return callbackRoute(error);
            } else {
                return callbackRoute(null);
            }
        });
};

var customerLogout = function (userData, callbackRoute) {
    async.series([
            function (callback) {
                var condition = {_id: userData.id};
                var dataToUpdate = {$unset: {accessToken: 1, deviceToken: 1}};
                Services.CustomerService.updateCustomer(condition, dataToUpdate, {new: true}, function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        callback();
                    }
                });
            }
        ],
        function (error, result) {
            if (error) {
                return callbackRoute(error);
            } else {
                return callbackRoute();
            }
        });
};

var verifyOTP = function (payload, tokenData, callback) {
    if (!payload || !tokenData._id) {
        callback(ERROR.IMP_ERROR)
    } else {
        //    var newNumberToVerify = queryData.countryCode + '-' + queryData.mobileNo;
        async.series([
            function (cb) {
                //Check verification code :
                if (payload.OTPCode == tokenData.OTPCode) {
                    cb();
                } else {
                    cb(ERROR.INVALID_CODE)
                }
            },
            function (cb) {
                //trying to update customer
                var criteria = {
                    _id: tokenData.id,
                    OTPCode: payload.OTPCode
                };
                var setQuery = {
                    $set: {isPhoneVerified: true},
                    $unset: {OTPCode: 1}
                };
                var options = {new: true};
                console.log('updating>>>', criteria, setQuery, options);
                Services.CustomerService.updateCustomer(criteria, setQuery, options, function (err, updatedData) {
                    console.log('verify otp callback result', err, updatedData);
                    if (err) {
                        cb(err)
                    } else {
                        if (!updatedData) {
                            cb(ERROR.INVALID_CODE)
                        } else {
                            cb();
                        }
                    }
                });
            }
        ], function (err, result) {
            if (err) {
                callback(err)
            } else {
                callback();
            }

        });
    }

};

var resendOTP = function (userData, callback) {

    var mobileNumber = userData.mobileNumber;
    var countryCode = userData.countryCode;
    var dataFound;
    if (!mobileNumber) {
        callback(ERROR.IMP_ERROR);
    } else {
        var uniqueCode = null;
        async.series([
            function (cb) {
                var query = {
                    mobileNumber: userData.mobileNumber
                };
                var projection = {
                    _id: 1,
                    mobileNumber: 1,
                    isPhoneVerified: 1,
                    countryCode: 1
                };
                Services.CustomerService.getCustomer(query, projection, {}, function (err, data) {
                    if (err) {
                        cb(ERROR.PASSWORD_CHANGE_REQUEST_INVALID);
                    } else {
                        dataFound = data && data[0] || null;
                        if (dataFound == null) {
                            cb(ERROR.PHONENUMBER_NOT_REGISTERED);
                        } else {
                            if (dataFound.isPhoneVerified == true) {
                                cb(ERROR.PHONE_VERIFICATION_COMPLETE);
                            } else {
                                cb();
                            }

                        }
                    }
                });
            },
            function (cb) {
                CodeGenerator.generateUniqueCode(4, Config.AppConstants.DATABASE.USER_ROLES.CUSTOMER, function (err, numberObj) {
                    if (err) {
                        cb(err);
                    } else {
                        if (!numberObj || numberObj.number == null) {
                            cb(ERROR.UNIQUE_CODE_LIMIT_REACHED);
                        } else {
                            uniqueCode = numberObj.number;
                            cb();
                        }
                    }
                })
            },
            function (cb) {
                var criteria = {
                    _id: userData.id
                };
                var setQuery = {
                    $set: {
                        OTPCode: uniqueCode
                    }
                };
                var options = {
                    lean: true
                };
                Services.CustomerService.updateCustomer(criteria, setQuery, options, cb);
                console.log("New OTTTTTTTTTTTTP Code", uniqueCode);
            }
            //, function (cb) {
            //    var criteria = {
            //        _id: userData.id
            //    };
            //    var projection = {
            //        OTPCode: 1
            //    };
            //    Services.CustomerService.getCustomer(criteria, projection, {lean: true}, cb);
            //}
        ], function (err, result) {
            callback(err, null);
        })
    }
};

var addAddress = function (payload, tokenData, callback) {
    var dataToSet = {};
    var address;
    async.series([
            function (cb) {
                dataToSet = {
                    customerId: tokenData.id,
                    customerLocation: {
                        "coordinates": [payload.longitude, payload.latitude],
                        "type": "Point"
                    },
                    streetAddress: payload.streetAddress,
                    apartmentNumber: payload.apartmentNumber,
                    postalCode: payload.postalCode,
                    country: payload.country,
                    state: payload.state,
                    city: payload.city
                };
                Services.AddressService.createAddress(dataToSet, function (err, data) {
                    if (!err) {
                        address = data;
                        cb(null);
                    }
                    else {
                        cb(err);
                    }
                });
            }
        ],
        function (error, result) {
            if (error) {
                callback(error);
            } else {
                callback(null, {address: address});
            }
        });
};

var getAddress = function (tokenData, callback) {
    var address;
    async.series([
            function (cb) {
                var query = {
                    customerId: tokenData.id,
                    isDeleted: false
                };
                Services.AddressService.getAddress(query, {__v: 0}, {lean: true}, function (err, data) {
                    console.log('$$$$$$$$$$$$$$', err, data);
                    if (!err) {
                        if (data.length > 0) {
                            address = data;
                            cb(null);
                        }
                        else {
                            cb(ERROR.ADDRESS_NOT_FOUND);
                        }
                    }
                    else {
                        cb(err);
                    }

                });
            }
        ],
        function (error, result) {
            if (error) {
                return callback(error);
            } else {
                return callback(null, {
                    address: address
                });
            }
        });
};

var deleteAddress = function (payload, tokenData, callbackRoute) {
    async.series([
            function (cb) {
                var query = {
                    customerId: tokenData.id,
                    _id: payload.addressId
                };
                var dataToUpdate = {$set: {isDeleted: true}};
                Services.AddressService.updateAddress(query, dataToUpdate, {}, function (err, data) {
                    if (!err) {
                        cb(null);
                    }
                    else {
                        cb(err);
                    }
                })
            }
        ],
        function (error, result) {
            if (error) {
                return callbackRoute(error);
            } else {
                return callbackRoute(null);
            }
        });
};

var registerViaFacebook = function (payload, callbackRoute) {
    var customerDoesNotExist = false;
    var returnedData = {};
    var accessToken = null;
    var uniqueCode = null;
    var dataToSave = payload;
    var customerData = null;
    var dataToUpdate = {};
    payload.email = payload.email.toLowerCase();
    async.series([
        function (cb) {
            //verify email address
            if (!UniversalFunctions.verifyEmailFormat(dataToSave.email)) {
                cb(ERROR.INVALID_EMAIL);
            } else {
                cb();
            }
        },
        function (cb) {
            var criteria = {
                $or: [{email: payload.email}, {mobileNumber: payload.mobileNumber}]
            };
            Services.CustomerService.getCustomer(criteria, {}, {lean: true}, function (err, resp) {
                if (err) {
                    cb(err)
                } else {
                    if (resp && resp.length != 0) {
                        cb(ERROR.USER_ALREADY_REGISTERED)
                    } else {
                        customerDoesNotExist = true;
                        cb()
                    }
                }
            })
        },
        function (cb) {
            CodeGenerator.generateUniqueCode(4, Config.AppConstants.DATABASE.USER_ROLES.CUSTOMER, function (err, numberObj) {
                if (err) {
                    cb(err);
                } else {
                    if (!numberObj || numberObj.number == null) {
                        cb(ERROR.UNIQUE_CODE_LIMIT_REACHED);
                    } else {
                        uniqueCode = numberObj.number;
                        cb();
                    }
                }
            })
        },
        function (cb) {
            //Insert Into DB
            dataToSave.OTPCode = uniqueCode;
            dataToSave.mobileNumber = payload.mobileNumber;
            Services.CustomerService.createCustomer(dataToSave, function (err, customerDataFromDB) {
                //Models.Customers(dataToSave).save(function (err, customerDataFromDB) {
                console.log('hello', err, customerDataFromDB);
                if (err) {
                    if (err.code == 11000 && err.message.indexOf('customers.$mobileNo_1') > -1) {
                        cb(ERROR.PHONE_NO_EXIST);

                    } else if (err.code == 11000 && err.message.indexOf('customers.$email_1') > -1) {
                        cb(ERROR.EMAIL_ALREADY_EXIST);

                    } else {
                        cb(err)
                    }
                } else {
                    customerData = customerDataFromDB;
                    cb();
                }
            });
        }, function (cb) {
            //Set Access Token
            if (customerData) {
                var tokenData = {
                    id: customerData._id,
                    type: Config.AppConstants.DATABASE.USER_ROLES.CUSTOMER
                };
                createToken.setToken(tokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        accessToken = output && output.accessToken || null;
                        cb();
                    }
                })
            } else {
                cb(ERROR.IMP_ERROR)
            }
        }
    ], function (err, data) {
        if (err) {
            callbackRoute(err)
        } else {
            var details = {
                customerDetails: dataToSave,
                accessToken: accessToken
            };
            return callbackRoute(null, details);
        }
    })
};

var loginViaFacebook = function (payload, callbackRoute) {
    var userFound = false;
    var successLogin = false;
    var accessToken = null;
    var returnedData = {};
    var updatedUserDetails = null;
    var defaultAddress = null;

    async.series([
        function (cb) {
            //verify email address
            if (!UniversalFunctions.verifyEmailFormat(dataToSave.email)) {
                cb(ERROR.INVALID_EMAIL);
            } else {
                cb();
            }
        },
        function (cb) {
            var criteria = {
                email: payload.email
                //$or: [{email: payload.emailOrPhone}, {mobileNumber: payload.emailOrPhone}]
            };
            var option = {
                lean: true
            };
            Services.CustomerService.getCustomer(criteria, {}, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    userFound = result && result[0] || null;
                    returnedData = result;
                    cb();
                }
            });
        },
        function (cb) {
            //validations
            if (!userFound) {
                //cb("You are not registered with us.");
                cb(ERROR.EMAIL_NOT_FOUND)
            } else {
                if (userFound && userFound.isPhoneVerified == false) {
                    cb(ERROR.NOT_VERFIFIED)
                }
                else {
                    //if (userFound && userFound.password != UniversalFunctions.CryptData(payload.password)) {
                    //    //cb("Incorrect Password");
                    //    cb(ERROR.INCORRECT_PASSWORD)
                    //} else {
                    successLogin = true;
                    cb();
                    //}
                }
            }
        },
        function (cb) {
            var query = {
                customer: userFound._id,
                isDefault: false,
                isDeleted: false
            };
            Services.AddressService.getAddress(query, {}, {lean: true}, function (err, data) {
                if (!err) {
                    if (data.length > 0) {
                        defaultAddress = data[0];
                        cb(null);
                    }
                    else {
                        cb(null);
                    }
                }
                else {
                    cb(err)
                }
            })
        },
        function (cb) {
            if (successLogin) {
                var tokenData = {
                    id: userFound._id,
                    type: Config.AppConstants.DATABASE.USER_ROLES.CUSTOMER
                };
                createToken.setToken(tokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        if (output && output.accessToken) {
                            accessToken = output && output.accessToken;
                            cb();
                        } else {
                            //cb("Error 2")
                            cb(ERROR.IMP_ERROR)
                        }
                    }
                })
            } else {
                cb(ERROR.IMP_ERROR)
                //cb("Error 3")
            }

        }
    ], function (err, data) {
        if (err) {
            callbackRoute(err)
        } else {
            var userDetails = UniversalFunctions.deleteUnnecessaryUserData(userFound);
            var details = {
                customerDetails: userDetails,
                accessToken: accessToken
            };
            callbackRoute(null, details);
        }
    })
};

module.exports = {
    customerRegister: customerRegister,
    customerLogin: customerLogin,
    getProfile: getProfile,
    changePassword: changePassword,
    customerLogout: customerLogout,
    verifyOTP: verifyOTP,
    resendOTP: resendOTP,
    addAddress: addAddress,
    getAddress: getAddress,
    deleteAddress: deleteAddress,
    registerViaFacebook: registerViaFacebook,
    loginViaFacebook: loginViaFacebook

};