/**
 * Created by SONY on 3/15/2016.
 */
var async = require('async');
var mongoose = require('mongoose');
var Models = require('../Models');
var Config = require('../Config');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var Services = require('../Services');
var ERROR = Config.AppConstants.STATUS_MSG.ERROR;

var addStatus = function (payload, tokenData, callbackRoute) {
    var customerData = {};
    async.series([
            function (cb) {
                var query = {
                    _id: tokenData.id
                };
                var status = {
                    statusText: payload.status
                };

                var dataToUpdate = {$push: {myStatus: status}};
                console.log("this is data");
                console.log(dataToUpdate);
                Services.CustomerService.updateCustomer(query, dataToUpdate, {lean: true}, function (err, user) {
                    //console.log("**************************",user);
                    console.log("customerData-------->>>" + JSON.stringify(user));
                    if (err) {
                        cb(err)
                    } else {
                        if (!user || user.length == 0) {
                            cb(ERROR.NOT_FOUND);
                        }
                        else {
                            customerData = user;
                            cb();
                        }
                    }
                });
            },
            function (cb) {
                var query = {
                    _id: tokenData.id
                };
                var projection = {
                    myStatus: 1,
                    id: 1
                };

                var options = {lean: true};
                Services.CustomerService.getCustomer(query, projection, options, function (err, data) {
                    if (err) {
                        cb(err)
                    } else {
                        customerData = data;
                        //customerData = data && data[0] || null;
                        console.log("customerData-------->>>" + JSON.stringify(customerData));
                        if (customerData == null) {
                            cb(ERROR.INVALID_TOKEN);
                        } else {
                            cb();
                        }
                    }
                });
            }
        ],
        function (error, result) {
            console.log("final call:", customerData);
            if (error) {
                return callbackRoute(error);
            } else {
                return callbackRoute(null, {customerData});
            }
        }
    );
};

var updateStatus = function (payload, tokenData, callbackRoute) {
    var customerData = {};
    async.series([
            function (callback) {
                var update = {
                    'myStatus.$.statusText': payload.status
                };
                var condition = {
                    _id: tokenData._id.toString(),
                    'myStatus': {$elemMatch: {_id: payload.statusId}}
                };
                Services.CustomerService.updateCustomer(condition, update, {multi: true}, function (err, status) {
                    console.log("customerData-------->>>" + JSON.stringify(status));
                    if (err) {
                        callback(err);
                    } else {
                        if (!status || status.length == 0) {
                            callback(ERROR.NOT_FOUND);
                        }
                        else {
                            callback();
                        }
                    }
                });
            }
        ],
        function (error, result) {
            if (error) {
                return callbackRoute(error);
            } else {
                return callbackRoute(null, result);
            }
        }
    );
};

module.exports = {
    addStatus: addStatus,
    updateStatus: updateStatus
};